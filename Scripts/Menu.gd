extends Control


signal btn_slots_pressed
signal btn_bonuses_pressed
signal btn_info_pressed


var view_h: float
var scrolling_container: MarginContainer
var scrolling_container_h: float
const TWEEN_SPEED: float = 0.15
var input_event_relative: float = 0.0


# BUILTINS - - - - - - - - -


func _ready() -> void:
	scrolling_container = $MarginScroll as MarginContainer
	if scrolling_container.is_visible_in_tree():
		view_h = get_viewport_rect().size.y
		scrolling_container_h = scrolling_container.get_rect().end.y


func _input(event: InputEvent) -> void:
	if scrolling_container.is_visible_in_tree():
		if scrolling_container_h > view_h:
			# вертикальный скроллинг плиток
			if event is InputEventScreenDrag:
				scrolling_container.rect_position.y += (event as InputEventScreenDrag).relative.y
				input_event_relative = (event as InputEventScreenDrag).relative.y
			# когда перестали скролить проверяем позицию плиток
			elif event is InputEventScreenTouch and not event.is_pressed():
				set_page()


# METHODS - - - - - - - - -


# выравнивание плиток, если они далеко проскролены
func set_page() -> void:
	var _t: bool
	if scrolling_container.get_rect().position.y > 0.0:
		_t = ($Tween as Tween).interpolate_property(scrolling_container, "rect_position:y", null, 0.0, TWEEN_SPEED)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()
		yield(get_tree().create_timer(TWEEN_SPEED + 0.1), "timeout")
		input_event_relative = 0.0
	elif scrolling_container.get_rect().position.y + scrolling_container.get_rect().size.y < view_h:
		_t = ($Tween as Tween).interpolate_property(scrolling_container, "rect_position:y", null, view_h - scrolling_container.get_rect().size.y, TWEEN_SPEED)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()
		yield(get_tree().create_timer(TWEEN_SPEED + 0.1), "timeout")
		input_event_relative = 0.0



func _on_BtnSlots_pressed() -> void:
	emit_signal("btn_slots_pressed")


func _on_BtnBonuses_pressed() -> void:
	emit_signal("btn_bonuses_pressed")


func _on_BtnInfo1_pressed() -> void:
	emit_signal("btn_info_pressed", 1)


func _on_BtnInfo2_pressed() -> void:
	emit_signal("btn_info_pressed", 2)