extends Control


signal btn_info_back_pressed


const TEXT: PoolStringArray = PoolStringArray([
	"One of the most popular types of gambling entertainment, which is an integral part of any self-respecting casino, is slot machines or slots. Even the most experienced players often prefer to play slot machines over blackjack or roulette. And there are several reasons for this: the slot game is truly gambling, since winning in it depends on pure luck, and the chances of winning for a professional and a beginner are absolutely equal. To play successfully, you do not need to memorize many rules and develop complex strategies. In addition, you can play slots at minimum stakes, and the winnings can amount to several thousand, or even millions of dollars, if it is a jackpot. With the development of high technologies, the \"one-armed bandit\" migrated from stuffy gaming halls to the vastness of the Internet, while playing became even more interesting, convenient and profitable. Any player can take a big risk, try online slot machines for free and enjoy gambling without leaving their own home.\n\nSlots are considered one of the most popular gambling entertainment. According to statistics, about 50% of all the world's offline and online casinos come to play slot machines. It is not surprising that when developing slots, manufacturers use the most advanced technologies and spend huge amounts of money on finding innovative solutions to make their products even more interesting and attractive to players.\n\nToday, online casinos can offer their users hundreds of different slot models: from classic to ultra-modern, with functional symbols and progressive jackpots, realistic 3D graphics and bonus rounds.\n\nAll currently existing slots can be divided into two large groups - offline and online.",
	"Mechanical - the first (classic) slot machines with three reels, one payline and traditional symbols (bars, sevens, cherries, bells), which are currently used, rather, to decorate the interior of gambling halls or exhibits in gambling museums. Now, as well as many years ago, these devices are called \"one-armed bandits\" because of the large lever that resembles a hand. The lever was used by the players to start the mechanism that set the drums in motion.\n\nSemi-mechanical slot machines are the next generation of slot machines, in which the rotation of the reels began to be controlled by a computer. Thanks to the improvement, semi-mechanical machines received several new options, such as multipliers, several pay lines, the ability to buy an option, accumulation of points, etc. To start the reels, the Spin button is used more often. True, some manufacturers continued to produce models with a lever - so to speak, a tribute to tradition. Semi-mechanical slots can still be found in gambling halls.\n\nVideo slots are the latest generation machines, in which real reels have been replaced by a screen with their image. Hence the prefix \"video\". The use of new technologies in the slot game opened up wide opportunities for creativity and each slot manufacturer used them in its own way. The playing field of the video began to include five, seven, nine reels with a huge number of pay lines (in some models more than 100). Bonus games, bank games and multi-games were added to the main game with the ability to select several games on one machine. The slots themselves have become themed, with beautiful graphics and animations, realistic sound effects and additional screens for mini-games.",
	"With the advent of online casinos, the developers not only transferred all the functions and characteristics of land-based slot machines to the online space, but also added a couple of new options to the list of slot game varieties.\n\nClassic slots are essentially a virtual analogue of the \"one-armed bandit\". Also, this concept can be attributed to any offline or online slot with three reels and one payline. Unlike real 3-reel slots, classic online slots can differ in their subject matter. Many developers have abandoned traditional symbols, replacing them with thematic ones. Also, 3-reel online slots can contain special symbols or bonus games and be included in the network with a progressive jackpot. The classic online slot often has a single payline, although there are exceptions here as well.\n\nMulti-reel slots are models of online slot machines, the playing field of which consists of 5, 7, 9 reels. Slots with an even number of reels are much less common.",
])


# BUILTINS -------------------------


# METHODS -------------------------


func set_content(index: int) -> void:
	($Scroll/Margin/TextInfo as Label).text = TEXT[index]


func reset_scroll() -> void:
	($Scroll as ScrollContainer).scroll_vertical = 0
	($Scroll/Margin/TextInfo as Label).rect_size.y = 0.0
	($Scroll/Margin as MarginContainer).rect_size.y = 0.0


# SETGET -------------------------


# SIGNALS -------------------------


func _on_BtnBack_pressed() -> void:
	emit_signal("btn_info_back_pressed")
