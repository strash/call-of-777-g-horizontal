extends Control


signal btn_bonus_pressed


var btn_bonus_1_pressed: bool = false
var btn_bonus_2_pressed: bool = false


# BUILTINS -------------------------


# METHODS -------------------------


# SETGET -------------------------


# SIGNALS -------------------------


func _on_Button1_pressed() -> void:
	if not btn_bonus_1_pressed:
		emit_signal("btn_bonus_pressed", 1000)
		btn_bonus_1_pressed = true
		var _i: bool = ($Tween as Tween).interpolate_property($VBoxContainer/Button1, "modulate:a", null, 0.3, 0.3)
		if not ($Tween as Tween).is_active():
			_i = ($Tween as Tween).start()


func _on_Button2_pressed() -> void:
	if not btn_bonus_2_pressed:
		emit_signal("btn_bonus_pressed", 20000)
		btn_bonus_2_pressed = true
		var _i: bool = ($Tween as Tween).interpolate_property($VBoxContainer/Button2, "modulate:a", null, 0.3, 0.3)
		if not ($Tween as Tween).is_active():
			_i = ($Tween as Tween).start()

