extends Control


signal btn_info_select_pressed


# BUILTINS -------------------------


# METHODS -------------------------


# SETGET -------------------------


# SIGNALS -------------------------


func _on_BtnInfo1_pressed() -> void:
	emit_signal("btn_info_select_pressed", 1)


func _on_BtnInfo2_pressed() -> void:
	emit_signal("btn_info_select_pressed", 2)


func _on_BtnInfo3_pressed() -> void:
	emit_signal("btn_info_select_pressed", 3)
